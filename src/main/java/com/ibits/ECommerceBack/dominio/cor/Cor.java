package com.ibits.ECommerceBack.dominio.cor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;


@Entity
@Table(name = "cor")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Cor implements Serializable{


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cor_id_seq")
    @SequenceGenerator(name = "cor_id_seq", sequenceName = "cor_id_seq", allocationSize = 1)
    @Column(name = "id")
    private long id;

    @NotNull
    @Column(name = "cor")
    private String cor;

    // GET & SET
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return cor;
    }

    public void setNome(String cor) {
        this.cor = cor;
    }
}