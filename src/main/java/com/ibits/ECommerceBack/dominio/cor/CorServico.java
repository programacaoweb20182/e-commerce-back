package com.ibits.ECommerceBack.dominio.cor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CorServico {

    private final CorRepositorio corRepositorio;

    @Autowired
    public CorServico(CorRepositorio corRepositorio) {
        this.corRepositorio = corRepositorio;
    }


    public Cor salvar(Cor cor){
        return corRepositorio.save(cor);
    }

    public boolean excluir(Long corId){
        Cor corBusca = corRepositorio.getOne(corId);
        if( corBusca != null ){
            corRepositorio.delete(corBusca);
            return true;
        }
        else {
            return false;
        }
    }

    public Cor buscarPeloId(Long corId){
        return corRepositorio.getOne(corId);
    }

    public List<Cor> buscarTodos(){
        return corRepositorio.findAll();
    }

}