package com.ibits.ECommerceBack.dominio.cor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CorRepositorio extends JpaRepository<Cor, Long> {
}
