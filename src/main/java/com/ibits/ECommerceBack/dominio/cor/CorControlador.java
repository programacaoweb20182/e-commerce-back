package com.ibits.ECommerceBack.dominio.cor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cor")
public class CorControlador {

    private final CorServico corServico;

    @Autowired
    public CorControlador(CorServico corServico) {
        this.corServico = corServico;
    }

    @PostMapping
    public ResponseEntity<?> salvar(@Validated @RequestBody Cor cor){
        System.out.println(cor);
        return  new ResponseEntity(corServico.salvar(cor), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<?> alterar(@Validated @RequestBody Cor cor){
        return new ResponseEntity(corServico.salvar(cor), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{corId}")
    public ResponseEntity<?> excluir(@PathVariable Long corId) {
        return new ResponseEntity(corServico.excluir(corId), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> buscarTodos() {
        return new ResponseEntity(corServico.buscarTodos(), HttpStatus.OK);
    }

    @GetMapping(value = "/{corId}")
    public ResponseEntity<?> findById(@PathVariable Long corId) {
        return new ResponseEntity(corServico.buscarPeloId(corId), HttpStatus.OK);
    }

}