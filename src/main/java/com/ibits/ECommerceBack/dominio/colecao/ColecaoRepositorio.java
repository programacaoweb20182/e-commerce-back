package com.ibits.ECommerceBack.dominio.colecao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ColecaoRepositorio extends JpaRepository<Colecao, Long> {
}
