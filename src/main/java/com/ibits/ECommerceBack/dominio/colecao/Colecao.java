package com.ibits.ECommerceBack.dominio.colecao;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ibits.ECommerceBack.dominio.produto.Produto;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "colecao")
@JsonIgnoreProperties({"produtos","hibernateLazyInitializer", "handler"})
public class Colecao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "colecao_id_seq")
    @SequenceGenerator(name = "colecao_id_seq", sequenceName = "colecao_id_seq", allocationSize = 1)
    @Column(name = "id")
    private long id;

    @NotNull
    @Column(name = "nome")
    private String nome;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "colecao")
    List<Produto> produtos;

    // GET & SET
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

}