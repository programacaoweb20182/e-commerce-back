package com.ibits.ECommerceBack.dominio.colecao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/colecao")
public class ColecaoControlador {

    private final ColecaoServico colecaoServico;

    @Autowired
    public ColecaoControlador(ColecaoServico colecaoServico) {
        this.colecaoServico = colecaoServico;
    }

    @PostMapping
    public ResponseEntity<?> salvar(@Validated @RequestBody Colecao colecao){
        System.out.println(colecao);
        return  new ResponseEntity(colecaoServico.salvar(colecao), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<?> alterar(@Validated @RequestBody Colecao colecao){
        return new ResponseEntity(colecaoServico.salvar(colecao), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{colecaoId}")
    public ResponseEntity<?> excluir(@PathVariable Long colecaoId) {
        return new ResponseEntity(colecaoServico.excluir(colecaoId), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> buscarTodos() {
        return new ResponseEntity(colecaoServico.buscarTodos(), HttpStatus.OK);
    }

    @GetMapping(value = "/{colecaoId}")
    public ResponseEntity<?> findById(@PathVariable Long colecaoId) {
        return new ResponseEntity(colecaoServico.buscarPeloId(colecaoId), HttpStatus.OK);
    }

}