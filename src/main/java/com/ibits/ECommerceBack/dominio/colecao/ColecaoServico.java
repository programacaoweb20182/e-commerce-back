package com.ibits.ECommerceBack.dominio.colecao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ColecaoServico {

    private final ColecaoRepositorio colecaoRepositorio;

    @Autowired
    public ColecaoServico(ColecaoRepositorio colecaoRepositorio) {
        this.colecaoRepositorio = colecaoRepositorio;
    }


    public Colecao salvar(Colecao colecao){
        return colecaoRepositorio.save(colecao);
    }

    public boolean excluir(Long colecaoId){
        Colecao colecaoBusca = colecaoRepositorio.getOne(colecaoId);
        if( colecaoBusca != null ){
            colecaoRepositorio.delete(colecaoBusca);
            return true;
        }
        else {
            return false;
        }
    }

    public Colecao buscarPeloId(Long colecaoId){
        return colecaoRepositorio.getOne(colecaoId);
    }

    public List<Colecao> buscarTodos(){
        return colecaoRepositorio.findAll();
    }

}