package com.ibits.ECommerceBack.dominio.contato;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContatoServico {

    private final ContatoRepositorio contatoRepositorio;

    @Autowired
    public ContatoServico(ContatoRepositorio contatoRepositorio) {
        this.contatoRepositorio = contatoRepositorio;
    }


    public Contato salvar(Contato contato){
        return contatoRepositorio.save(contato);
    }

    public boolean excluir(Long contatoId){
        Contato contatoBusca = contatoRepositorio.getOne(contatoId);
        if( contatoBusca != null ){
            contatoRepositorio.delete(contatoBusca);
            return true;
        }
        else {
            return false;
        }
    }

    public Contato buscarPeloId(Long contatoId){
        return contatoRepositorio.getOne(contatoId);
    }

    public List<Contato> buscarTodos(){
        return contatoRepositorio.findAll();
    }

}