package com.ibits.ECommerceBack.dominio.contato;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/contato")
public class ContatoControlador {

    private final ContatoServico contatoServico;

    @Autowired
    public ContatoControlador(ContatoServico contatoServico) {
        this.contatoServico = contatoServico;
    }

    @PostMapping
    public ResponseEntity<?> salvar(@Validated @RequestBody Contato contato){
        System.out.println(contato);
        return  new ResponseEntity(contatoServico.salvar(contato), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<?> alterar(@Validated @RequestBody Contato contato){
        return new ResponseEntity(contatoServico.salvar(contato), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{contatoId}")
    public ResponseEntity<?> excluir(@PathVariable Long contatoId) {
        return new ResponseEntity(contatoServico.excluir(contatoId), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> buscarTodos() {
        return new ResponseEntity(contatoServico.buscarTodos(), HttpStatus.OK);
    }

    @GetMapping(value = "/{contatoId}")
    public ResponseEntity<?> findById(@PathVariable Long contatoId) {
        return new ResponseEntity(contatoServico.buscarPeloId(contatoId), HttpStatus.OK);
    }

}